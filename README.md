# FitBit Insight+
Authors: Benjamin Berkhout en Wytze Bekker

This folder contains a complete local webapplication to visualize data from a Fitbit Smartwatch.


# REQUIREMENTS
To use this local webapplication you will need to install:
Java 11.0.9
Tomcat 9.0.27
Thymeleaf 3.0.11
IntelliJ 2019.3.2 



* To run this webapplication, clone the repository to your desired location.
* Open the project in IntelliJ.
* On the top right, click "Edit configurations". 
* On the popup screen, click the + button on the top left and select Tomcat Server > Local. 
* Under the "Server" tab, add "/home" to the URL so it results in "http://localhost:8080/home"
* Under the "Deployment" tab, click "Fix" and change Application context to "/"
* Click okay and click "Run application". The application should now open in a browser. 


The application can only process JSON files.
The data necessary to run this application can be downloaded from the fitbit website and comes in JSON format.
This application will not upload your data to outside your pc but strictly remain local. 