fetch('/avg_storage')
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        console.log(myJson)
        console.log("Values:");
        let date = myJson.map(t => {
            return t.date
        });

        let avgBpm = myJson.map(b => {
            return b.avg
        });
        console.log(avgBpm)


        let ctx = document.getElementById('avgRates').getContext('2d');
        let  avgRates = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: date,
                datasets: [{
                    label: 'Average heart rate',
                    data: avgBpm,
                    backgroundColor: 'rgb(150,161,252)',
                    pointRadius: 0
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Daily average heartrate'
                },
            },
        });

    });