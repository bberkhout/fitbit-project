/**
 * Usage:
 *  Add an SVG element (with the ID set to 'steps') to the page including this JS file:
 *    <svg width="1111" height="350" id="resting">
 *
 *  Change the path to the data source in the 'ds.json' call below. See bottom of this file
 *  for a data example.
 */

d3.json('/json_storage').then(function(data) {
    data = data[0].heartRate;
    data = data.map(d => {
        let event = new Date(d.timestamp);
        return {
            dateTime: event.toLocaleDateString("en-US"),
            bpm: d.bpm
        };
    });

    let svg = d3.select("#resting"),
        margin = {top: 20, right: 20, bottom: 110, left: 40},
        margin2 = {top: 280, right: 20, bottom: 30, left: 40},
        width = +svg.attr("width") - margin.left - margin.right,
        height = +svg.attr("height") - margin.top - margin.bottom,
        height2 = +svg.attr("height") - margin2.top - margin2.bottom;

    let parseDate = d3.timeParse("%m/%d/%Y");

    /* TODO: make dynamic */
    brushYearStart= "2019-12-05";
    brushYearEnd = "2019-12-05";

    let brushYears = svg.append("g");
    brushYears.append("text")
        .attr("id", "brushYears")
        .classed("yearText", true)
        .text(brushYearStart + " - " + brushYearEnd)
        .attr("font-family", "sans-serif")
        .attr("font-size", "12")
        .attr("x", 35)
        .attr("y", 12);

    let x = d3.scaleTime().range([0, width]),
        x2 = d3.scaleTime().range([0, width]),
        y = d3.scaleLinear().range([height, 0]),
        y2 = d3.scaleLinear().range([height2, 0]);

    let xAxis = d3.axisBottom(x),
        xAxis2 = d3.axisBottom(x2),
        yAxis = d3.axisLeft(y);

    let brush = d3.brushX()
        .extent([[0, 0], [width, height2]])
        .on("brush end", brushed);

    function updateDateRange(brush) {
        let extent = brush.extent();
        let rangeExtent = [x2( extent[0] ), x2( extent[1] ) ];
    }

    let zoom = d3.zoom()
        .scaleExtent([1, Infinity])
        .translateExtent([[0, 0], [width, height]])
        .extent([[0, 0], [width, height]])
        .on("zoom", zoomed);

    let area = d3.area()
        .curve(d3.curveMonotoneX)
        .x(function (d) {
            return x(parseDate(d.dateTime));
        })
        .y0(height)
        .y1(function (d) {
            return y(d.bpm);
        });

    let area2 = d3.area()
        .curve(d3.curveMonotoneX)
        .x(function (d) {
            return x2(parseDate(d.dateTime));
        })
        .y0(height2)
        .y1(function (d) {
            return y2(d.bpm);
        });

    svg.append("defs").append("clipPath")
        .attr("id", "clip")
        .append("rect")
        .attr("width", width)
        .attr("height", height);

    let focus = svg.append("g")
        .attr("class", "focus")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    let context = svg.append("g")
        .attr("class", "context")
        .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

    // Data Management
    x.domain(d3.extent(data, function(d) { return parseDate(d.dateTime); }));
    y.domain([
        Math.round(d3.min(data, function(d) { return d.bpm; })) - 2,
        Math.round(d3.max(data, function(d) { return d.bpm; })) + 2
    ]);

    x2.domain(x.domain());
    y2.domain(y.domain());

    focus.append("path")
        .datum(data)
        .attr("class", "area")
        .attr("d", area);

    focus.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    focus.append("g")
        .attr("class", "axis axis--y")
        .call(yAxis);

    context.append("path")
        .datum(data)
        .attr("class", "area")
        .attr("d", area2);

    context.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height2 + ")")
        .call(xAxis2);

    context.append("g")
        .attr("class", "brush")
        .call(brush)
        .call(brush.move, x.range());

    svg.append("rect")
        .attr("class", "zoom")
        .attr("width", width)
        .attr("height", height)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .call(zoom);

    function brushed() {
        if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
        let s = d3.event.selection || x2.range();
        x.domain(s.map(x2.invert, x2));
        focus.select(".area").attr("d", area);
        focus.select(".axis--x").call(xAxis);
        svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
            .scale(width / (s[1] - s[0]))
            .translate(-s[0], 0));
    }

    function zoomed() {
        if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
        let t = d3.event.transform;
        x.domain(t.rescaleX(x2).domain());
        focus.select(".area").attr("d", area);
        focus.select(".axis--x").call(xAxis);
        context.select(".brush").call(brush.move, x.range().map(t.invertX, t));
    }
});

