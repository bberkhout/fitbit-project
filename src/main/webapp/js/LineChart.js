let iter = 1;


fetch('/json_storage')
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        let hrdata = myJson[0].heartRate;

        let timestamp = hrdata.map(t => { return t.timestamp });

        let bpm = hrdata.map( b => {return b.bpm});



        //Making random colors when adding a new dataset
        let dynamicColors = function() {
            let r = Math.floor(Math.random() * 255);
            let g = Math.floor(Math.random() * 255);
            let b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
        };

        //Function to get each day of the dataset
        function getHeartDays(dates){
            let heartDays = [];
            for (let i = 0; i < myJson.length; i++){
                dates = myJson[i].heartRate;
                let days = dates.map(t => { return t.timestamp });
                heartDays.push(new Date(days[0]).toLocaleDateString())
            }
            return heartDays;
        }

        let days = getHeartDays(timestamp);

        //Making a dynamic select list containing each day from the set
        let select = document.getElementById("selectHeartDate");
        for(index in days) {
            select.options[select.options.length] = new Option(days[index], days[index]);
        }




        //First initial data for the line chart
        let data = {
            labels: timestamp,
            datasets: [{
                label: new Date(timestamp[0]).toLocaleDateString(),
                data: bpm,
                borderColor: 'rgba(255,0,55,0.25)',
                borderWidth: 1,
                pointRadius: 0
            }]
        };


        //make the chart
        let ctx = document.getElementById('canvas').getContext('2d');
        let canvas = new Chart(ctx, {
            type: 'line',
            data: data,
            options: {
                spanGaps: false,
                pointRadius: 10,
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Heartrate over the course of a day'
                },
                legend: {
                    position: 'left',
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            unit: 'hour'
                        }
                    }]
                }
            }
        });


        //Add the data selected in the select form
        function addSelectHeartData(day) {
            for (let i = 0; i < myJson.length; i++) {
                let dates = myJson[i].heartRate;
                let days = dates.map(t => {
                    return new Date(t.timestamp).toLocaleDateString()
                });

                if (iter < myJson.length) {
                    if (day === days[0]) {
                        let newHeartData = myJson[i].heartRate;
                        let newBpm = newHeartData.map(b => {
                            return b.bpm
                        });
                        let newTimestamp = newHeartData.map(t => {
                            return t.timestamp
                        });

                        let newData = {
                            label: new Date(newTimestamp[0]).toLocaleDateString(),
                            data: newBpm,
                            borderColor: dynamicColors(),
                            borderWidth: 1,
                            pointRadius: 0,
                        };
                        data.datasets.push(newData);
                    }
                }
                else{
                    alert("All datasets are already added!")
                }
            }
            iter++;
            canvas.update();
        }

        //Remove the selected day
        function removeHeartData(day) {
            if (iter === 0) {
                iter++;
                alert("Can't delete only Dataset!")
            } else {
                for (let i = 0; i < myJson.length; i++) {
                    let dates = myJson[i].heartRate;
                    let days = dates.map(t => {
                        return new Date(t.timestamp).toLocaleDateString()
                    });
                    if (day === days[0]) {
                        data.datasets.splice(i, 1);
                        canvas.update();
                    }
                }
                iter--;
            }
        }


        function getSelectedData(){
            let e = document.getElementById("selectHeartDate");
            let result = e.options[e.selectedIndex].value;

            addSelectHeartData(result)
        }

        function removeSelectData(){
            let e = document.getElementById("selectHeartDate");
            let result = e.options[e.selectedIndex].value;
            removeHeartData(result)

        }

        document.getElementById('addSelectHeartDay').onclick = getSelectedData;
        document.getElementById('removeHeartDay').onclick = removeSelectData;

    });


