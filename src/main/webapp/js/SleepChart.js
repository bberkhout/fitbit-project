fetch('/day_sleep_storage')
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        let sleepData = myJson[0];


        let light = sleepData.map(t => { return t.light });
        let wake = sleepData.map(w => {return w.wake});
        let deep = sleepData.map(de => {return de.deep});
        let date = sleepData.map(d => {return d.date});
        let restless = sleepData.map(rl => {return rl.restless});
        let rem = sleepData.map(re => {return re.rem});

        //Dataset for each phase a different color
        let stack_data = {
            labels: date,
            datasets: [{
                label: 'Light',
                backgroundColor: 'rgb(153, 204, 255)',
                data: getValFromList(light)
            } ,{
                label: 'Deep',
                backgroundColor: 'rgb(150,161,252)',
                data: getValFromList(deep)
        }, {
                label: 'REM',
                backgroundColor: 'rgb(177,176,255)',
                data: getValFromList(rem)
            }, {
                label: 'restless',
                backgroundColor: 'rgb(255,178,102)',
                data: getValFromList(restless)
            }, {
                label: 'wake',
                backgroundColor: 'rgb(174,255,174)',
                data: getValFromList(wake)
            },
            ]
        };
        console.log(stack_data);

        function getValFromList(dataList){
            var valueList = [];
            for (let i = 0; i < dataList.length; i++){
                valueList.push(dataList[i])
            }
            return valueList;
        }


        let ctx = document.getElementById('sleepProc').getContext('2d');
        let  sleepProc = new Chart(ctx, {
            type: 'bar',
            data: stack_data,
            options: {
                title: {
                    display: true,
                    text: 'Sleep level distribution in minutes'
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });

    });