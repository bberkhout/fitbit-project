let iter_new = 1;

fetch('/flow_sleep_storage')
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        let sleepData = myJson[0][0];
        console.log(sleepData);

        let timestamp = sleepData.map(t => { return t.dateTime });

        let level = sleepData.map( b => {return b.level});

        //Changing the different phases to numeric values
        function changeLevel(levels){
            let adjustLevel = [];
            for (let i =0; i < levels.length; i++){
                switch (levels[i]) {
                    case 'wake':
                        adjustLevel.push(5);
                        break;
                    case 'light':
                        adjustLevel.push(4);
                        break;
                    case 'deep':
                        adjustLevel.push(1);
                        break;
                    case 'rem':
                        adjustLevel.push(3);
                        break;
                    case 'restless':
                        adjustLevel.push(1.5);
                        break;
                }
            }
            return adjustLevel;
        }

        let adjustedLevels = changeLevel(level);
        console.log(level);

        let yLabel ={
            5 : 'Wake', 4 : 'Light', 1 : 'Deep', 3 : 'REM', 1.5 : 'restless'
        };

        //Function to get each day of the dataset
        function getDays(dates){
            let diffDays = [];
            for (let i = 0; i < myJson[0].length; i++){
                dates = myJson[0][i];
                let days = dates.map(t => { return t.dateTime });
                diffDays.push(new Date(days[0]).toLocaleDateString())
            }
            return diffDays;
        }

        let days = getDays(timestamp);

        //Making a dynamic select list containing each day from the set
        let select = document.getElementById("selectDate");
        for(index in days) {
            select.options[select.options.length] = new Option(days[index], days[index]);
        }

        console.log(getDays(timestamp));

        //Making random colors when adding a new dataset
        let dynamicColors = function() {
            let r = Math.floor(Math.random() * 255);
            let g = Math.floor(Math.random() * 255);
            let b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
        };



        //First initial data for the line chart
        let data = {
            labels: timestamp,
            datasets: [{
                label: new Date(timestamp[0]).toLocaleDateString(),
                data: adjustedLevels,
                borderColor: 'rgb(0,3,130)',
                borderWidth: 1,
                pointRadius: 0,
                steppedLine: true
            }]
        };


        let ctx = document.getElementById('sleepFlow').getContext('2d');
        let sleepFlow = new Chart(ctx, {
            type: 'line',
            data: data,
            options: {
                spanGaps: false,
                pointRadius: 10,
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Sleep level lapse'
                },
                legend: {
                    position: 'left',
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            unit: 'hour'
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            callback: function(value, index, values) {
                                return yLabel[value];
                            }
                        }
                    }]
                }
            }
        });


        // //add the next data in the dataset
        // function addNextSleepData() {
        //     if (iter_new < myJson[0].length) {
        //         let newSleepData = myJson[0][iter_new];
        //         let newLevels = newSleepData.map( b => {return b.level});
        //         let newTimestamp = newSleepData.map(t => { return t.dateTime });
        //
        //
        //         let newAdjustedLevels = changeLevel(newLevels);
        //
        //         let newData = {
        //             label: new Date(newTimestamp[0]).toLocaleDateString(),
        //             data: newAdjustedLevels,
        //             borderColor: dynamicColors(),
        //             borderWidth: 1,
        //             pointRadius: 0,
        //             steppedLine: true
        //         };
        //         data.datasets.push(newData);
        //         iter_new++;
        //
        //         sleepFlow.update();
        //     }
        //     else{
        //         alert("All datasets are already added!")
        //     }
        // }

        //Add the data selected in the select form
        function addSelectSleepData(day) {

            for (let i = 0; i < myJson[0].length; i++) {
                let dates = myJson[0][i];
                let days = dates.map(t => {
                    return new Date(t.dateTime).toLocaleDateString()
                });

                console.log("current day: " + days[0]);
                console.log("Selected day: " + day);
                if (iter < myJson[0].length) {
                    if (day === days[0]) {

                        let newSleepData = myJson[0][i];
                        let newLevels = newSleepData.map(b => {
                            return b.level
                        });
                        let newTimestamp = newSleepData.map(t => {
                            return t.dateTime
                        });


                        let newAdjustedLevels = changeLevel(newLevels);

                        let newData = {
                            label: new Date(newTimestamp[0]).toLocaleDateString(),
                            data: newAdjustedLevels,
                            borderColor: dynamicColors(),
                            borderWidth: 1,
                            pointRadius: 0,
                            steppedLine: true
                        };
                        data.datasets.push(newData);
                    }
                }
                else{
                    alert("All datasets are already added!")
                }


            }
                iter_new++;
                sleepFlow.update();
        }

        function removeSleepData(day) {
            if (iter_new === 0) {
                iter_new++;
                alert("Can't delete only Dataset!")
            } else {
                for (let i = 0; i < myJson[0].length; i++) {
                    let dates = myJson[0][i];
                    let days = dates.map(t => {
                        return new Date(t.dateTime).toLocaleDateString()
                    });
                    if (day === days[0]) {
                        data.datasets.splice(i, 1);
                        sleepFlow.update();
                    }
                }
                iter_new--;
            }
        }


        function getSelectedData(){
            let e = document.getElementById("selectDate");
            let result = e.options[e.selectedIndex].value;

            addSelectSleepData(result)
        }

        function removeSelectData(){
            let e = document.getElementById("selectDate");
            let result = e.options[e.selectedIndex].value;

            removeSleepData(result)

        }

        document.getElementById('addSelectData').onclick = getSelectedData;
        document.getElementById('removeSleepData').onclick = removeSelectData;


    });


