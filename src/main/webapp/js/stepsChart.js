
fetch('/steps_storage')
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        let stepsData = myJson[0].Steps;
        let timeSteps = stepsData.map(t => { return new Date(t.date) });

        let steps = stepsData.map( b => {return b.steps});


        let ctx = document.getElementById('StepsChart').getContext('2d');
        let StepsChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: timeSteps,
                datasets: [{
                    label: '# of steps',
                    data: steps,
                    backgroundColor: 'rgba(255,0,55,0.25)',
                    pointRadius: 0,
                    spanGaps: false,
                    steppedLine: true
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Course of steps'
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            unit: 'day'
                        }
                    }]
                }
            }
        });
    });