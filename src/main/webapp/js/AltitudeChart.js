
fetch('/alt_storage')
    .then((response) => {
        return response.json();
    })
    .then((myJson) => {
        let altData = myJson[0].altitudes;
        let timestamp = altData.map(t => { return new Date(t.date) });
        console.log(timestamp);

        let altitude = altData.map( b => {return b.alt});


        let ctx = document.getElementById('AltitudeChart').getContext('2d');
        let AltitudeChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: timestamp,
                datasets: [{
                    label: 'Altitude',
                    data: altitude,
                    backgroundColor: 'rgba(255,0,55,0.25)',
                    pointRadius: 0
                }]
            },
            options: {
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            unit: 'day'
                        }
                    }]
                }
            }
        });


    });