package nl.bioinf.bberkhout.servlets;

import nl.bioinf.bberkhout.DataInitializer;
import nl.bioinf.bberkhout.FileProcesser;
import nl.bioinf.bberkhout.WebConfig;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//moved to web.xml
@MultipartConfig(location="/tmp",
        fileSizeThreshold = 1024 * 1024 * 1024,
        maxFileSize = 1024 * 1024 * 50,
        maxRequestSize = 1024 * 1024 * 5 * 5)
@WebServlet(name = "FileSelectServlet", urlPatterns = "/fileSelect")
public class FileSelectServlet extends HttpServlet {

    private TemplateEngine templateEngine;
    private String uploadDir;

    private int maxMemSize = 1024 * 1024 * 50;
    private int maxFileSize = 1024 * 1024 * 50;
    File file ;
    private String FileName = null;
    private String FilePath = null;
    private String tempFilePath = null;


    @Override
    public void init() throws ServletException {
        final ServletContext servletContext = super.getServletContext();
        this.templateEngine = WebConfig.createTemplateEngine(servletContext);
        this.uploadDir = "/homes/bberkhout/Desktop"; //getInitParameter("upload_dir");

        //Making new temporary folder at the current working directory
        String currentDir = System.getProperty("user.dir");
        String tempMap = "/temp_data";
        FilePath = currentDir + tempMap;

        //Make a File type object for the mkdir() function
        File newPath = new File(FilePath);

        System.out.println("new created path: " + FilePath);

        try {
            //Check if a temporary folder already exists
            if (Files.exists(Path.of(FilePath))){
                //Delete old temporary folder
                FileUtils.deleteDirectory(new File(FilePath));
                System.out.println("Temporary folder already exists, deleting now");
                newPath.mkdir();
            }
            else{
                System.out.println("Creating temp folder at: " + FilePath);
                newPath.mkdir();
            }
        }
        catch (Exception ex){
            System.err.println(ex);
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        templateEngine.process("fileSelect", ctx, response.getWriter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
       factory.setRepository(new File(this.uploadDir));

       ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax( maxFileSize );

        try{
            System.out.println("Starting file upload...");
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);
            // Process the uploaded file items
            Iterator filesIter = fileItems.iterator();
            do {
                FileItem fi = (FileItem)filesIter.next();

                // save original filename
                this.FileName = fi.getName();
                //create a unique file name to prevent replacing old files
//                this.UniqueFileName =  RandomStringUtils.randomAlphanumeric(15) + "_" + FileName;
                this.tempFilePath = this.FilePath + File.separator + this.FileName;

                file = new File(this.tempFilePath) ;
                fi.write(file) ;
                System.out.println("uploaded " + FileName + " to destination: " + this.FilePath);

                //Calling FileProcess to process JSON
                ServletContext context = getServletContext();
                context.setAttribute("filePath", FilePath);


            } while (filesIter.hasNext());
            DataInitializer.initializeDataSource();
        }
        catch (Exception ex){
            System.err.println(ex);
        }

        File[] files = new File(FilePath).listFiles();
        List<String> fileNames = new ArrayList<String>();


        for(File file : files) {
            if (file.isFile()) {
                String fileName = file.getName();
                fileNames.add(fileName);
            }
        }
        System.out.println();

        FileProcesser fileProcesser = DataInitializer.getDataSource();

        WebContext ctx = new WebContext(
                request,
                response,
                request.getServletContext(),
                request.getLocale());
        ctx.setVariable("FileName", fileNames);
        ctx.setVariable("HeartRateList", fileProcesser.getFileList());
        templateEngine.process("result", ctx, response.getWriter());
    }
}
