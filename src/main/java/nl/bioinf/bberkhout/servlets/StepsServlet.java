package nl.bioinf.bberkhout.servlets;

import nl.bioinf.bberkhout.DataInitializer;
import nl.bioinf.bberkhout.FileProcesser;
import org.thymeleaf.TemplateEngine;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "StepsServlet", urlPatterns = "/steps_storage", loadOnStartup = 1)
public class StepsServlet extends HttpServlet {
    private TemplateEngine templateEngine;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final FileProcesser fileProcesser = DataInitializer.getDataSource();

        String data = fileProcesser.getSteps().toString();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(data);



    }
}