package nl.bioinf.bberkhout;

public class DataInitializer {
    private static FileProcesser fileProcesser;

    public static void initializeDataSource() {
        if (fileProcesser != null) {
            throw new IllegalStateException("FileProcessor already exists!");
        } else {
            createFileProcessor();
        }
    }

    public static FileProcesser getDataSource(){
        if (fileProcesser == null){
            throw new IllegalStateException("data must first be initialized!");
        }
        return fileProcesser;
    }

    private static void createFileProcessor() {
        String filePath = System.getProperty("user.dir") + "/temp_data";
        try {
            fileProcesser = new FileProcesser(filePath);
        }
        catch (Exception ex){
            System.err.println("Something went wrong with processing the data");
        }
    }

}
