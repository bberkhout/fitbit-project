package nl.bioinf.bberkhout;

import java.io.File;
import java.io.FileReader;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.google.gson.Gson;

public class FileProcesser {

    ArrayList<String> procJSON = new ArrayList<String>();
    ArrayList<String> HeartAvg = new ArrayList<>();
    ArrayList<String> Altitudes = new ArrayList<>();
    ArrayList<String> Steps = new ArrayList<>();
    ArrayList fileList = new ArrayList();
    ArrayList<String> SleepSumms = new ArrayList<String>();
    ArrayList<String> SleepFlow = new ArrayList<>();

    public FileProcesser(String pathDirectory) throws Exception {



        System.out.println("Starting file process.. ");

        final String dir = System.getProperty("user.dir");
        System.out.println("Current dir: " + dir);

        List<String> fileList = new ArrayList<String>();

        File[] files = new File(pathDirectory).listFiles();
        Arrays.sort(files);
        for(File file : files){
            if (file.isFile()){
                String fileName = file.getName();
                String[] fileNameStrip = fileName.split("-");
                //Sorting the received files and process them using different functions
                switch (fileNameStrip[0]){
                    case "heart_rate":
                        this.procJSON.add(heartRateProcess(file));
                        this.HeartAvg.add(HeartAvarage(file));
                        fileList.add("Heartrate on " + fileNameStrip[1] +"/"+ fileNameStrip[2] +"/"+ fileNameStrip[3]);
                        break;
                    case "time_in_heart_rate_zones":
                        break;
                    case "altitude":
                        this.Altitudes.add(getAltitudes(file));
                        break;
                    case "calories":
                        break;
                    case "distance":
                        break;
                    case "lightly_active_minutes":
                        break;
                    case "moderately_active_minutes":
                        break;
                    case "sedentary_minutes":
                        break;
                    case "sleep":
                        this.SleepSumms.add(getSleep(file));
                        this.SleepFlow.add(sleepFlow(file));
                        break;
                    case "steps":
                        this.Steps.add(getSteps(file));
                        break;
                    case "swim_lengths":
                        break;
                    case "very_active_minutes":
                        break;
                }
            }
        }

    }

    //Function to process the files containing heartrate data
    private String heartRateProcess(File file) throws Exception{

        //Creating object
        Object obj = new JSONParser().parse(new FileReader(file));
        JSONArray jo = (JSONArray) obj;

        String[] outFileDate = null;
        String outFileName = null;

        ArrayList outList = new ArrayList();

        Map outMap = new HashMap();

        for (Object item : jo) {
            JSONObject jsonObject = (JSONObject) item;
            String firstDate = (String) jsonObject.get("dateTime");

            //Get the timestamp
            String date = createDate(firstDate);

            //Get the BPM
            Map values = ((Map) jsonObject.get("value"));

            //Add time and bpm to little map
            Map lilOutMap = new HashMap();
            lilOutMap.put("timestamp", date );
            lilOutMap.put("bpm", values.get("bpm"));

            outList.add(lilOutMap);
            //Create the name for the output file
            if (outFileDate == null) {
                outFileDate = firstDate.split(" ");
                outFileName = "heartRate"; // + outFileDate[0];
            }

        }
        //Add the name and the list to a map
        outMap.put(outFileName, outList);

        //Convert the  map to a JSON format
        Gson outGSON = new Gson();
        String outJSONobj = outGSON.toJson(outMap);

        return outJSONobj;
    }

    //Function to process the files containing heartrate data and calculate the avarage per day
    private String HeartAvarage (File file) throws Exception {
        Integer hrSum = 0;
        Integer hrAvg;
        String date = null;

        Object obj = new JSONParser().parse(new FileReader(file));
        JSONArray jo = (JSONArray) obj;

        for (Object item : jo) {
            JSONObject jsonObject = (JSONObject) item;
            //Get value, here: bpm
            Map values = ((Map) jsonObject.get("value"));
            Long bpm = (Long) values.get("bpm");
            //Add the bpms up
            hrSum = hrSum + bpm.intValue();
            //Get date
            if(date == null) {
                String datetime = (String) jsonObject.get("dateTime");
                String[] splitdatetime = datetime.split(" ");
                date = splitdatetime[0];
            }
        }
        //Calculate avarage heartrate by sum/length
        hrAvg = hrSum / jo.size();
        //Add time and value to little map
        Map<String, java.io.Serializable> lilAvgMap = new HashMap<>();
        lilAvgMap.put("date", date);
        lilAvgMap.put("avg", hrAvg);
        //Convert map to json
        Gson outGSON = new Gson();
        String outJSONavg = outGSON.toJson(lilAvgMap);

        return outJSONavg;
    }

    //Function to process the files containing altitude data
    private String getAltitudes(File file) throws Exception {
        ArrayList<Map<String, String>> altList = new ArrayList<>();
        Map<String, ArrayList<Map<String, String>>> outAltMap = new HashMap<String, ArrayList<Map<String, String>>>();
        String outObjName = null;

        Object obj = new JSONParser().parse(new FileReader(file));
        JSONArray jo = (JSONArray) obj;

        for (Object item : jo) {
            JSONObject jsonObject = (JSONObject) item;
            //Get value, here: altitudes
            String alt = (String) jsonObject.get("value");
            //Get time
            String datetime = (String) jsonObject.get("dateTime");
            String date = createDate(datetime);

            //Add time and value to little map
            Map<String, String> lilAltMap = new HashMap<>();
            lilAltMap.put("date", date);
            lilAltMap.put("alt", alt);
            //Add little map to list
            altList.add(lilAltMap);
            if (outObjName == null) {
                outObjName = "altitudes" + date;
            }
        }
        //Add list to a map as value with key "altitudes"
        outAltMap.put("altitudes", altList);
        //Convert map to json
        Gson outGSON = new Gson();
        String outJSONobj = outGSON.toJson(outAltMap);

        return outJSONobj;
    }

    //Function to process the files containing step data
    private String getSteps(File file) throws Exception {
        Integer stepsSum = 0;
        ArrayList<Map<String, java.io.Serializable>> stepList = new ArrayList<Map<String, java.io.Serializable>>();
        Map<String, ArrayList<Map<String, java.io.Serializable>>> outStepMap = new HashMap<String, ArrayList<Map<String, java.io.Serializable>>>();
        String checkdate;
        String prevdate = new String();
        Object obj = new JSONParser().parse(new FileReader(file));
        JSONArray jo = (JSONArray) obj;

        for (Object item : jo) {
            JSONObject jsonObject = (JSONObject) item;
            //Get value, here: steps
            String steps = (String) jsonObject.get("value");
            //Add to total steps

            //Get time
            String datetime = (String) jsonObject.get("dateTime");
            String date = createDate(datetime);

            //Reset stepsum per day
            String[] checkdatetime = date.split("T");
            checkdate = checkdatetime[0];
            if(prevdate.equals("")){
                prevdate = checkdate;
            }
            if(checkdate.equals(prevdate)) {
                stepsSum = stepsSum + Integer.parseInt(steps);
            }
            if (!checkdate.equals(prevdate)) {
                stepsSum = Integer.parseInt(steps);
                prevdate = checkdate;
            }


            //Add time and value to little map
            Map<String, java.io.Serializable> lilStepMap = new HashMap<>();
            lilStepMap.put("date", date);
            lilStepMap.put("steps", stepsSum);
            //Add little map to list
            stepList.add(lilStepMap);
        }
        //Add list to a map as value with key "Steps"
        outStepMap.put("Steps", stepList);
        //Convert map to json
        Gson outGSON = new Gson();
        String outJSONobj = outGSON.toJson(outStepMap);
        return outJSONobj;
    }

    //Function to process the files containing sleep data to create a stacked barplot
    private String getSleep(File file) throws Exception {

        ArrayList<Map<String, Object>> sleepsumms = new ArrayList<>();

        Object obj = new JSONParser().parse(new FileReader(file));
        JSONArray jo = (JSONArray) obj;
        for(Object item : jo) {
            JSONObject jsonObject = (JSONObject) item;

            Map<String, Object> summ = new HashMap<String, Object>();

            String date = (String) jsonObject.get("dateOfSleep");
            JSONObject levels = (JSONObject) jsonObject.get("levels");
            JSONObject summary = (JSONObject) levels.get("summary");
            summ.put("date", date);
            //Deep sleep
            JSONObject deepsleep = (JSONObject) summary.get("deep");
            if (deepsleep != null) {
                summ.put("deep", deepsleep.get("minutes"));
            } else {
                summ.put("deep", 0);
            }

            //Wake
            JSONObject wake = (JSONObject) summary.get("wake");
                if (wake != null){
                    summ.put("wake", wake.get("minutes"));
                } else {
                    summ.put("wake", 0);
                }
            //Light
            JSONObject light = (JSONObject) summary.get("light");
            if (light != null){
                summ.put("light", light.get("minutes"));
            } else {
                summ.put("light", 0);
            }
            //Rem
            JSONObject rem = (JSONObject) summary.get("rem");
            if (rem != null){
                summ.put("rem", rem.get("minutes"));
            } else {
                summ.put("rem", 0);
            }
            //Restless
            JSONObject restless = (JSONObject) summary.get("restless");
            if (restless != null){
                summ.put("restless", restless.get("minutes"));
            } else {
                summ.put("restless", 0);
            }
            sleepsumms.add(summ);
        }

        Gson outGSON = new Gson();
        String outJSONobjsleep = outGSON.toJson(sleepsumms);

        return outJSONobjsleep;
    }

    //Function to process the files containing sleep data to create a sleepflow diagram
    private String sleepFlow(File file) throws Exception {

        ArrayList outSleepFlowMap = new ArrayList();

        Object obj = new JSONParser().parse(new FileReader(file));
        JSONArray jo = (JSONArray) obj;
        for(Object item : jo) {
            ArrayList outSleepFlow = new ArrayList();
            JSONObject jsonObject = (JSONObject) item;
            String date = (String) jsonObject.get("dateOfSleep");
            JSONObject levels = (JSONObject) jsonObject.get("levels");
            ArrayList data = (ArrayList) levels.get("data");

            for (Object compItem : data) {
                Map compItemMap = (HashMap) compItem;

                compItemMap.remove("seconds");

                outSleepFlow.add(compItemMap);
            }
            outSleepFlowMap.add(outSleepFlow);
        }

        Gson outGSON = new Gson();
        String outJSONobjsleepflow = outGSON.toJson(outSleepFlowMap);
        return outJSONobjsleepflow;
    }

    //Function to extract the date from samples
    private String createDate(String date){

        String[] splitdatetime = date.split(" ");
        String onlyDate = splitdatetime[0];

        String[] dateSplit = onlyDate.split("/");
        String dd = dateSplit[1] + "T";
        String mm = dateSplit[0] + "-";
        String yyyy = "20" + dateSplit[2] + "-";

        String correctDate = yyyy + mm + dd + splitdatetime[1] + "Z";

        return correctDate;
    }

    //Getters for each arraylist containing the processed data
    public ArrayList<String> getHeartAvg() {
        return HeartAvg;
    }

    public ArrayList<String> getAltitudes() {
        return Altitudes;
    }

    public ArrayList<String> getSteps() {
        return Steps;
    }

    public ArrayList getFileList() {
        return fileList;
    }

    public ArrayList<String> getSleepSumms() { return SleepSumms; }

    public ArrayList<String> getProcJSON() {
        return procJSON;
    }

    public ArrayList<String> getSleepFlow() {
        return SleepFlow;
    }
}



